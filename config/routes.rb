# frozen_string_literal: true

require "sidekiq/web"
require "sidekiq/cron/web"

Rails.application.routes.draw do
  mount Sidekiq::Web => "/sidekiq", :constraints => Constraints::Auth.new
  mount Yabeda::Prometheus::Exporter => "/metrics" if AppConfig.metrics

  root AppConfig.anonymous_access ? "projects#index" : "sessions#new"

  get "sign_in", to: "sessions#new"
  post "sign_in", to: "sessions#create"
  delete "logout", to: "sessions#destroy"

  resources :projects, only: %i[index create update]

  put "/jobs/:id/execute", to: "job#execute", as: "job_execute"

  namespace :api, defaults: { format: :json } do
    resources :hooks, only: [:create]
    resources :notify_release, only: [:create]
    resources :projects, only: %i[index show create update destroy]
    scope :projects do
      resources :registration, only: [:create] if AppConfig.project_registration == "system_hook"
    end
  end

  Healthcheck.routes(self)
end
