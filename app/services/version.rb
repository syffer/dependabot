# frozen_string_literal: true

class Version
  VERSION = "0.34.0"
end
