# Deploy modes

There are several ways to deploy `dependabot-gitlab` app.

## Helm chart

The preferred deployment type is deploying on kubernetes cluster using official [helm chart](https://gitlab.com/dependabot-gitlab/chart).

Add helm repo:

```sh
helm repo add dependabot https://dependabot-gitlab.gitlab.io/chart
```

Install application:

```sh
helm install dependabot dependabot/dependabot-gitlab --values values.yaml
```

For the full list of configurable options, see [values](https://gitlab.com/dependabot-gitlab/chart#values).

## Docker compose

It is possible to use [docker-compose.yml](https://gitlab.com/dependabot-gitlab/dependabot/-/blob/main/docker-compose.yml) to start the app using [docker compose](https://docs.docker.com/compose/) `up` command:

```sh
docker compose up -d
```

All of the app specific options have to be configured manually using environment variables. For the full list of options see [environment](../config/environment.md) configuration section.

## Standalone

Application can be used in a stateless cli like mode. This mode is most useful to run dependency updates from [Gitlab CI](https://docs.gitlab.com/ee/ci/).

[dependabot-gitlab/dependabot-standalone](https://gitlab.com/dependabot-gitlab/dependabot-standalone) describes the best way to run dependency updates from gitlab ci.

Like [docker compose](#docker-compose), all configuration is done via [environment variables](../config/environment.md).

Due to simple stateless setup, standalone mode does not support many of the more advanced features like:

- security vulnerability detection
- automatic closure of superseded merge requests
- merge request commands
- webhooks
- UI with managed project list
