include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml

# ======================================================================================================================
# Runners
# ======================================================================================================================
.bundle_install:
  script:
    - unset BUNDLE_APP_CONFIG
    - bundle config set --local path vendor/bundle
    - bundle install

.npm_install:
  script:
    - npm ci --cache .npm

.large_runner:
  tags:
    - ${LARGE_RUNNER_TAG}

.gem_cache: &gem_cache
  key:
    files:
      - Gemfile.lock
  paths:
    - vendor/bundle
  policy: pull

.node_cache: &node_cache
  key:
    files:
      - package-lock.json
  paths:
    - .npm
  policy: pull

.coverage_cache: &coverage_cache
  key: coverage-$CODACY_VERSION
  paths:
    - codacy-coverage-reporter
  policy: pull

.ruby_runner:
  variables:
    BUNDLE_SUPPRESS_INSTALL_USING_MESSAGES: "true"
  before_script:
    - !reference [.bundle_install, script]
  cache:
    - *gem_cache

.node_runner:
  image: registry.gitlab.com/dependabot-gitlab/ci-images:node-16 # node image extends ruby so it will have ruby as well
  before_script:
    - !reference [.npm_install, script]
  cache:
    - *node_cache

.docker_runner:
  image: registry.gitlab.com/dependabot-gitlab/ci-images:docker-20.10
  services:
    - name: docker:20.10.22-dind
      alias: docker
  tags:
    - docker
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_BUILDKIT: 1
  before_script:
    - source .gitlab/script/utils.sh
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin

.rspec_runner:
  stage: test
  extends: .ruby_runner
  services:
    - name: bitnami/redis:7.0-debian-11
      alias: redis
    - name: bitnami/mongodb:6.0-debian-11
      alias: mongodb
  variables:
    REDIS_URL: redis://redis:6379
    REDIS_PASSWORD: $REDIS_PASSWORD
    MONGODB_URL: mongodb:27017
    MAX_ROWS: 5
    OUTPUT_STYLE: block
    COVERAGE: "true"
    COV_DIR: reports/coverage/${CI_JOB_NAME}
  script:
    - |
      bundle exec rspec \
        --tag ${RSPEC_TAGS} \
        --format documentation \
        --format RspecJunitFormatter --out tmp/rspec.xml
  cache:
    - *gem_cache
  artifacts:
    reports:
      junit: tmp/rspec.xml
    paths:
      - reports/coverage
      - log/*.log
    expire_in: 1 day
    when: always

.snyk_runner:
  stage: static analysis
  image: registry.gitlab.com/dependabot-gitlab/ci-images:docker-20.10-snyk-1.9
  extends:
    - .docker_runner

# ======================================================================================================================
# Jobs
# ======================================================================================================================

# ----------------------------------------------------------------------------------------------------------------------
# .pre
#
.dont-interrupt:
  stage: .pre
  image: alpine:latest
  interruptible: false
  script: echo "I make sure this pipeline is not canceled!"

.cache_dependencies:
  stage: .pre
  extends: .node_runner
  variables:
    BUNDLE_FROZEN: "true"
    BUNDLE_SUPPRESS_INSTALL_USING_MESSAGES: "true"
  before_script:
    - !reference [.bundle_install, script]
    - !reference [.npm_install, script]
  script:
    - .gitlab/script/download-coverage.sh
  cache:
    - <<: *gem_cache
      policy: pull-push
    - <<: *node_cache
      policy: pull-push
    - <<: *coverage_cache
      policy: pull-push

# ----------------------------------------------------------------------------------------------------------------------
# build stage
#
.compile_assets:
  stage: build
  extends: .ruby_runner
  script:
    - bundle exec rake assets:precompile
  artifacts:
    when: on_success
    expire_in: 1 day
    paths:
      - public/assets

.build_app_image:
  stage: build
  extends:
    - .docker_runner
    - .large_runner
  timeout: 180m
  before_script:
    - !reference [.docker_runner, before_script]
    - setup_buildx
    - |
      if [[ "$BUILD_PLATFORM" =~ arm64 ]]; then
        install_qemu
      fi
  script:
    - .gitlab/script/build-image.sh

.build_core_image:
  stage: build
  extends:
    - .docker_runner
    - .large_runner
  needs: []
  timeout: 180m
  before_script:
    - !reference [.docker_runner, before_script]
    - install_qemu
    - setup_buildx
  script:
    - .gitlab/script/build-core-image.sh

.build_docs:
  stage: build
  extends:
    - .node_runner
  script:
    - npm run "docs:build"
  artifacts:
    paths:
      - vitepress

# ----------------------------------------------------------------------------------------------------------------------
# 'static analysis' stage
#
.rubocop:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec rubocop --parallel --color

.reek:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec reek --color --progress --force-exclusion --sort-by smelliness .

.brakeman:
  stage: static analysis
  extends: brakeman-sast

.dependency_scan:
  stage: static analysis
  extends: gemnasium-dependency_scanning

.license_scan:
  stage: static analysis
  extends: license_scanning
  variables:
    LICENSE_FINDER_CLI_OPTS: --enabled-package-managers=bundler
  cache:
    key:
      prefix: license
      files:
        - Gemfile.lock
    paths:
      - .gitlab/cache

.container_scan:
  stage: static analysis
  extends:
    - .snyk_runner
    - .ruby_runner
  script:
    - bundle exec rake "ci:container_scan[$APP_IMAGE]"

# ----------------------------------------------------------------------------------------------------------------------
# test stage
#
.unit-test:
  extends: .rspec_runner
  variables:
    RSPEC_TAGS: ~system

.system-test:
  extends: .rspec_runner
  services:
    - name: ${MOCK_IMAGE}
      alias: smocker
    - name: bitnami/redis:7.0-debian-10
      alias: redis
    - name: bitnami/mongodb:5.0-debian-10
      alias: mongodb
  variables:
    MOCK_HOST: smocker
    GITLAB_URL: http://${MOCK_HOST}:8080
    RSPEC_TAGS: system
  before_script:
    - !reference [.ruby_runner, before_script]
    - .gitlab/script/build-core-helpers.sh bundler

.standalone-test:
  stage: test
  extends:
    - .docker_runner
    - .large_runner
  variables:
    COMPOSE_PROJECT_NAME: dependabot
    METRICS_REPORT: metrics.txt
  script:
    - .gitlab/script/run-standalone.sh
  after_script:
    - curl -X POST -s "http://docker:8081/sessions/verify" | jq
    - |
      echo "# TYPE update_duration_seconds_sum summary" > $METRICS_REPORT
      echo "update_duration_seconds_sum $(cat time.txt)" >> $METRICS_REPORT
  artifacts:
    expire_in: 1 day
    when: always
    reports:
      metrics: $METRICS_REPORT

.deploy-test:
  stage: test
  extends:
    - .docker_runner
    - .large_runner
  variables:
    SETTINGS__GITLAB_URL: http://gitlab:8080
    SETTINGS__DEPENDABOT_URL: http://ci-test.com
    SETTINGS__GITLAB_ACCESS_TOKEN: e2e-test
    SETTINGS__GITHUB_ACCESS_TOKEN: e2e-test
    SETTINGS__PROJECT_REGISTRATION: automatic
    SETTINGS__LOG_COLOR: "true"
    SETTINGS__METRICS: "true"
    COMPOSE_PROJECT_NAME: dependabot
    REDIS_EXTRA_FLAGS: "--protected-mode no"
  before_script:
    - .gitlab/script/run-deploy.sh
  script:
    - .gitlab/script/test-deploy.sh
  after_script:
    - .gitlab/script/log-deploy.sh

.migration-test:
  extends: .deploy-test
  before_script: []
  after_script: []
  script:
    - .gitlab/script/test-migration.sh

# ----------------------------------------------------------------------------------------------------------------------
# test report stage
#
.coverage:
  stage: report
  extends: .ruby_runner
  variables:
    MAX_ROWS: 5
    OUTPUT_STYLE: block
    NO_COLOR: 1
  coverage: /^COVERAGE:\s+(\d{1,3}\.\d{1,2})\%/
  when: always
  script:
    - bundle exec rake "ci:merge_coverage"
    - |
      ./codacy-coverage-reporter report \
        --commit-uuid ${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-$CI_COMMIT_SHA} \
        --coverage-reports coverage/coverage.xml \
        --language ruby
  cache:
    - *coverage_cache
    - *gem_cache
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/coverage.xml

# ----------------------------------------------------------------------------------------------------------------------
# release stage
#
.release_image:
  stage: release
  extends: .docker_runner
  variables:
    DOCKERHUB_IMAGE: docker.io/andrcuns/dependabot-gitlab
    GITLAB_IMAGE: $CI_REGISTRY_IMAGE
  before_script:
    - !reference [.docker_runner, before_script]
    - echo "$DOCKERHUB_PASSWORD" | docker login -u "$DOCKERHUB_USERNAME" --password-stdin
  script:
    - .gitlab/script/release-image.sh
  interruptible: false

.gitlab_release:
  extends: .ruby_runner
  stage: release
  variables:
    CHANGELOG_FILE: release_notes.md
  script:
    - bundle exec rake "release:changelog[$CI_COMMIT_TAG,$CHANGELOG_FILE]"
  release:
    tag_name: $CI_COMMIT_TAG
    description: $CHANGELOG_FILE
  interruptible: false

.update_chart:
  extends: .ruby_runner
  stage: release
  script:
    - bundle exec rake "release:chart[$CI_COMMIT_TAG]"
  interruptible: false

.update_standalone:
  extends: .ruby_runner
  stage: release
  script:
    - bundle exec rake "release:standalone[$CI_COMMIT_TAG]"
  interruptible: false

.publish_docs:
  stage: release
  image: eeacms/rsync:2.4
  script:
    - rsync -ravh --delete vitepress/ public/
  interruptible: false
  artifacts:
    paths:
      - public

# ----------------------------------------------------------------------------------------------------------------------
# deploy stage
#
.deploy:
  stage: deploy
  trigger:
    project: dependabot-gitlab/deploy
    strategy: depend
